
CC = gcc
SOURCES = src/*.c
TARGET = bin/wushu

LINKER_FLAGS = -lmingw32 -lSDL2main -lSDL2
COMPILER_FLAGS = -Wall

all:
	$(CC) $(SOURCES) -o $(TARGET) $(COMPILER_FLAGS) $(LINKER_FLAGS)
