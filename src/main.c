#include "framework.h"
#include "wushu.h"
#include <stdlib.h>
#include <stdio.h>

void cleanup(void)
{
	game_destroy();
	engine_destroy();
}

int main(int argc, char* args[])
{
	atexit(cleanup);
	if(!engine_init(640, 480))
		return 1;
	
	if(!game_init())
		return 1;
	
	Timer ups_timer;
    timer_start(&ups_timer);

    float timer = 0.0f;
    float update_timer = 0.0f;
    float update_ticks = 1.0f / 60.0f;
    uint frames = 0;
    uint updates = 0;

    Timer delta_timer;
    timer_start(&delta_timer);
    float delta;	
	
	//mainloop
	while(should_close() != 1)
	{
		update_events();
		float ticks = timer_get_ticks(&ups_timer) / 1000.0f;
        if (ticks - update_timer > update_ticks)
        {
            delta = timer_get_ticks(&delta_timer) / 1000.0f;
            update_keys();            
			game_update(delta);								
            updates++;
            update_timer += update_ticks;
            timer_start(&delta_timer);
        }		
		
		//render
		start_frame();
		game_render();
		end_frame();
		frames++;
		
		if (ticks - timer > 1.0f)
        {
            timer += 1.0f;
            char buf[64];
            snprintf(buf, sizeof(buf), "wushu ups: %u fps: %u", updates, frames);            
            printf("%s\n", buf);            
            frames = 0;
            updates = 0;
        }
	}
	return 0;
}