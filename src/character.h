#ifndef _CHARACTER_H
#define _CHARACTER_H

#include "framework.h"

#define MAX_FRAMES 64
#define MAX_COLL_BOXES 4

typedef struct
{
	SDL_Rect src;
	SDL_Rect off_box[MAX_COLL_BOXES];
	int num_offence_boxes;
	SDL_Rect def_box[MAX_COLL_BOXES];
	int num_defence_boxes;
}Frame;

typedef struct
{
	Frame frames[MAX_FRAMES];
	int num_frames;
	int cur_frame;
	float cur_time;
}Animation;

typedef enum
{
	ENEMY_NORMAL = 0,
	ENEMY_STRONG,
	ENEMY_DAGGER,
	ENEMY_BOSS,
	NUM_ENEMY_TYPES
}EnemyType;

typedef struct
{
	EnemyType type;
	float pos_x;
	float pos_y;
	Animation animation;
}Enemy;

typedef struct
{
	int pos_x;
	int pos_y;
}Spawner;

#endif