#include "wushu.h"

#include "intro_state.h"
#include "menu_state.h"
#include "game_state.h"
#include "assets.h"


//=============================================
//STATES
//=============================================
typedef enum
{
	STATE_INTRO = 0,
	STATE_MENU,
	STATE_GAME,
	NUM_STATES
}GameState;

static GameState g_state;



int game_init()
{
	if(!assets_init())
		return 1;
	
	if(!init_intro_state())
		return 1;
	
	if(!init_menu_state())
		return 1;
	
	if(!init_game_state())
		return 1;
	
	g_state = STATE_MENU;
	
	
	return 1;
}

void game_destroy()
{
	destroy_intro_state();	
	destroy_menu_state();	
	destroy_game_state();
	assets_destroy();
}

void game_update(float delta)
{
	switch(g_state)
	{
		case STATE_INTRO:{
			int res = update_intro_state(delta);
			if(res != -1)
			{
				g_state = STATE_MENU;
			}
		}break;
		case STATE_MENU:{
			int res = update_menu_state(delta);
			if(res == 0)
			{
			
			}
			else if(res == 1)
			{
				//exit
			}
		}break;
		case STATE_GAME:{
			update_game_state(delta);
		}break;
		default:
			break;
	}	
}

void game_render()
{
	switch(g_state)
	{
		case STATE_INTRO:{
			render_intro_state();
		}break;
		case STATE_MENU:{
			render_menu_state();
		}break;
		case STATE_GAME:{
			render_game_state();
		}break;
		default:
			break;
	}
	
	
}



