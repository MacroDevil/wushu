#include "game_state.h"

//=============================================
//STAGE
//=============================================
static Stage g_stage;

//=============================================
//PLAYER
//=============================================
static float g_player_x;
static float g_player_y;

static Animation g_player_anim;


int init_game_state()
{
	if(!stage_load(&g_stage, 0))
	{
		return 0;
	}
	
	g_player_x = 30;
	g_player_y = 150;
	
	g_player_anim.cur_frame = 0;
	g_player_anim.cur_time = 0;
	g_player_anim.frames[0].src.x = 0;
	g_player_anim.frames[0].src.y = 0;
	g_player_anim.frames[0].src.w = 20;
	g_player_anim.frames[0].src.h = 40;
	g_player_anim.frames[0].num_offence_boxes = 0;
	g_player_anim.frames[0].num_defence_boxes = 0;
	g_player_anim.num_frames = 1;
	
	return 1;
}

void destroy_game_state()
{
	stage_destroy(&g_stage);
}

int update_game_state(float delta)
{
	//movel left
	if(is_key_down(BUTTON_LEFT))
	{
		g_player_x -= 20 * delta;
	}
	
	//move right
	if(is_key_down(BUTTON_RIGHT))
	{
		g_player_x += 20 * delta;
	}
	
	//crouch
	if(is_key_down(BUTTON_DOWN))
	{
		
	}
	
	//punch
	if(is_key_pressed(BUTTON_A))
	{
		
	}
	
	//kick
	if(is_key_pressed(BUTTON_X))
	{
	}
	
	//jump
	if(is_key_pressed(BUTTON_B))
	{
	}

	g_player_anim.cur_time += delta;
	if(g_player_anim.cur_time > 1.0f)
	{
		g_player_anim.cur_time = 0;
		g_player_anim.cur_frame++;
		if(g_player_anim.cur_frame >= g_player_anim.num_frames)
		{
			g_player_anim.cur_frame = 0;
		}
	}
	
	stage_update(&g_stage, delta);
	
	return -1;
}

void render_game_state()
{
	stage_render(&g_stage,0);
	
	SDL_Rect rect = {g_player_x-5, g_player_y-10,10,10};
	SDL_Color col = {0x00, 0xFF, 0x00, 0xFF};
	
	draw_rect(rect,col);
	draw_text("Test line\nsecond line bla",&g_font,10,10);
}