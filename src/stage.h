#ifndef _STAGE_H
#define _STAGE_H

#include "framework.h"
#include "character.h"

#define MAP_VERT_TILES 10
#define MAP_HOR_TILES 64
#define MAP_TILES MAP_VERT_TILES * MAP_HOR_TILES
#define MAX_SPAWNER 16

typedef struct
{	
	SDL_Rect src;
	int passable;
}Tile;

typedef struct
{
	SDL_Texture* tilemap;
	Tile tiles[MAP_TILES];
	
	Spawner spawner[MAX_SPAWNER];
	int num_spawner;
}Stage;

void stage_set_tile(Stage* stage, int x, int y, Tile tile);
Tile* stage_get_tile(Stage* stage, int x, int y);

void stage_destroy(Stage* stage);
void stage_update(Stage* stage, float delta);
void stage_render(Stage* stage, int scroll);
int stage_load(Stage* stage, int id);

#endif
