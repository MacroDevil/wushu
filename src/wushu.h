#ifndef _WUSHU_H
#define _WUSHU_H

#include "stage.h"

int game_init();
void game_destroy();
void game_update(float delta);
void game_render();

#endif