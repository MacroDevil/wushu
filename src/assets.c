#include "assets.h"

static Font g_font;

int assets_init()
{
	if(!font_create_default(&g_font))
		return 0;
	
	return 1;
}

void assets_destroy()
{
	SDL_DestroyTexture(g_font.texture);	
}

Font* get_font()
{
	return &g_font;
}