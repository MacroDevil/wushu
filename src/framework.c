#include "framework.h"

#include <string.h>

//====================================
//TIMER
//====================================

Timer timer_create()
{
	Timer res = {0};
	return res;	
}

void timer_start(Timer* timer)
{
    timer->is_paused = 0;
    timer->is_started = 1;
    timer->paused_ticks = 0;
    timer->started_ticks = SDL_GetTicks();
}

void timer_stop(Timer* timer)
{
    timer->is_paused = 0;
    timer->is_started = 0;
    timer->paused_ticks = 0;
    timer->started_ticks = 0;
}

void timer_pause(Timer* timer)
{
    if (timer->is_started && !timer->is_paused)
    {
        timer->is_paused = 1;
        timer->paused_ticks = SDL_GetTicks();
        timer->started_ticks = 0;
    }
}

void timer_unpause(Timer* timer)
{
    if (timer->is_started && timer->is_paused)
    {
        timer->is_paused = 0;
        timer->paused_ticks = 0;
        timer->started_ticks = SDL_GetTicks();
    }
}

uint timer_get_ticks(Timer* timer)
{
    uint res = 0;

    if (timer->is_started)
    {
        if (timer->is_paused)
        {
            res = timer->paused_ticks;
        }
        else
        {
            res = SDL_GetTicks() - timer->started_ticks;
        }
    }

    return res;
}

//====================================
//VARIABLES
//====================================
static SDL_Window* g_window;
static SDL_Renderer* g_renderer;

static int g_should_close;

const Uint8* g_keystate;
static SDL_GameController* g_controller;
static int g_keys_down[NUM_BUTTONS];
static int g_keys_pressed[NUM_BUTTONS];
static int g_keys_released[NUM_BUTTONS];

//====================================
//ENGINE
//====================================

int engine_init(int width, int height)
{
	g_should_close = 0;
	
	if(SDL_Init(SDL_INIT_EVERYTHING) != 0)
	{
		return 0;
	}
	
	int pos = SDL_WINDOWPOS_CENTERED;
    g_window = SDL_CreateWindow("wushu", pos, pos, width, height, SDL_WINDOW_SHOWN);
    if (!g_window)
    {
        printf("Cannot create window\n");
        return 0;
    }
	
	g_renderer = SDL_CreateRenderer(g_window, -1, SDL_RENDERER_ACCELERATED);
	
	SDL_RenderSetLogicalSize(g_renderer,320,180);
	SDL_SetRenderDrawColor( g_renderer, 0x00, 0x00, 0x00, 0xFF );  
	
	g_keystate = SDL_GetKeyboardState(NULL);
	
	g_controller = SDL_GameControllerOpen(0);
	if(!g_controller)
	{
		printf("no controller found\n");		
	}
	else
	{		
		const char* name = SDL_GameControllerName(g_controller);
		printf("controller found %s\n",name);
	}
	
	return 1;
}

void engine_destroy()
{
	SDL_GameControllerClose(g_controller);
	SDL_DestroyRenderer(g_renderer);
	SDL_DestroyWindow(g_window);
	SDL_Quit();
}
	
int should_close()
{
	return g_should_close;
}
	
void update_events()
{
	SDL_Event evt;
	while(SDL_PollEvent(&evt))
	{
		switch(evt.type)
		{
			case SDL_QUIT:
				g_should_close = 1;
				break;
            case SDL_KEYUP:
                if(evt.key.keysym.sym == SDLK_ESCAPE)
                {
                    g_should_close = 1;
                }
			default:
				break;
		}
	}
}

void start_frame()
{
	SDL_RenderClear(g_renderer);
}

void end_frame()
{
	SDL_RenderPresent(g_renderer);
}

void draw_rect(SDL_Rect rect, SDL_Color color)
{
	SDL_SetRenderDrawColor( g_renderer, color.r, color.g, color.b, color.a );        
	SDL_RenderDrawRect( g_renderer, &rect);
	SDL_SetRenderDrawColor( g_renderer, 0x00, 0x00, 0x00, 0xFF );  
}

void draw_filledrect(SDL_Rect rect, SDL_Color color)
{
	SDL_SetRenderDrawColor( g_renderer, color.r, color.g, color.b, color.a );
	SDL_RenderFillRect( g_renderer, &rect );
	SDL_SetRenderDrawColor( g_renderer, 0x00, 0x00, 0x00, 0xFF );  
}

void draw_sprite(SDL_Texture* texture, SDL_Rect src, SDL_Rect dst)
{	
	SDL_RenderCopy( g_renderer, texture, &src, &dst );
}

void draw_text(const char* text, Font* font, int x, int y)
{
	int pos_x = x;
	int pos_y = y;
	
	int len = strlen(text);
	for(int i=0; i<len; i++)
	{
		char letter = text[i];
		if(letter == '\n')
		{
			pos_y += 8;
			pos_x = x;
			continue;
		}
		
		if(letter == ' ')
		{
			pos_x += 8;
			continue;
		}
		
		SDL_Rect* src = &font->letters[(int)letter];
		SDL_Rect dst = {pos_x, pos_y, 8, 8};
		
		SDL_RenderCopy( g_renderer, font->texture, src, &dst );
		pos_x += 8;
	}
}

SDL_Texture* texture_load(const char* filename)
{
	SDL_Surface* surface = SDL_LoadBMP(filename);
	if(!surface)
	{
		printf("cannot load image %s\n",filename);
		return NULL;
	}
	
	SDL_SetColorKey( surface, SDL_TRUE, SDL_MapRGB( surface->format, 0xFF, 0x00, 0xFF ) );
		
	SDL_Texture* new_texture = SDL_CreateTextureFromSurface( g_renderer, surface );
    if( new_texture == NULL )
    {
		printf( "Unable to create texture from %s! SDL Error: %s\n", filename, SDL_GetError() );
	}
	
	SDL_FreeSurface( surface );
	return new_texture;
}

//====================================
//INPUT
//====================================

int is_controller_key_down(Button button)
{
	if(!g_controller)
	{
		return 0;
	}
	
	switch(button)
	{
		case BUTTON_A:{
			return SDL_GameControllerGetButton(g_controller, SDL_CONTROLLER_BUTTON_A) == 1;
		}break;
		case BUTTON_B:{
			return SDL_GameControllerGetButton(g_controller, SDL_CONTROLLER_BUTTON_B) == 1;
		}break;
		case BUTTON_X:{
			return SDL_GameControllerGetButton(g_controller, SDL_CONTROLLER_BUTTON_X) == 1;
		}break;
		case BUTTON_Y:{
			return SDL_GameControllerGetButton(g_controller, SDL_CONTROLLER_BUTTON_Y) == 1;
		}break;
		case BUTTON_L1:{
			return SDL_GameControllerGetButton(g_controller, SDL_CONTROLLER_BUTTON_LEFTSHOULDER) == 1;
		}break;
		case BUTTON_R1:{
			return SDL_GameControllerGetButton(g_controller, SDL_CONTROLLER_BUTTON_RIGHTSHOULDER) == 1;
		}break;
		case BUTTON_START:{
			return SDL_GameControllerGetButton(g_controller, SDL_CONTROLLER_BUTTON_START) == 1;
		}break;
		case BUTTON_LEFT:{
			int dpad = SDL_GameControllerGetButton(g_controller, SDL_CONTROLLER_BUTTON_DPAD_LEFT) == 1;
			int axis = SDL_GameControllerGetAxis(g_controller, SDL_CONTROLLER_AXIS_LEFTX) < -8000;
			return dpad || axis;
		}break;
		case BUTTON_RIGHT:{
			int dpad = SDL_GameControllerGetButton(g_controller, SDL_CONTROLLER_BUTTON_DPAD_RIGHT) == 1;
			int axis = SDL_GameControllerGetAxis(g_controller, SDL_CONTROLLER_AXIS_LEFTX) > 8000;
			return dpad || axis;
		}break;
		case BUTTON_UP:{
			int dpad = SDL_GameControllerGetButton(g_controller, SDL_CONTROLLER_BUTTON_DPAD_UP) == 1;
			int axis = SDL_GameControllerGetAxis(g_controller, SDL_CONTROLLER_AXIS_LEFTY) < -8000;
			return dpad || axis;
		}break;
		case BUTTON_DOWN:{
			int dpad = SDL_GameControllerGetButton(g_controller, SDL_CONTROLLER_BUTTON_DPAD_DOWN) == 1;
			int axis = SDL_GameControllerGetAxis(g_controller, SDL_CONTROLLER_AXIS_LEFTY) > 8000;
			return dpad || axis;
		}break;
		default:
		break;
	}
	return 0;
}

int is_keyboard_key_down(Button button)
{
	switch(button)
	{
		case BUTTON_A:{
			return g_keystate[SDL_SCANCODE_K] == 1;
		}break;
		case BUTTON_B:{
			return g_keystate[SDL_SCANCODE_L] == 1;
		}break;
		case BUTTON_X:{
			return g_keystate[SDL_SCANCODE_J] == 1;
		}break;
		case BUTTON_Y:{
			return g_keystate[SDL_SCANCODE_I] == 1;
		}break;
		case BUTTON_L1:{
			return g_keystate[SDL_SCANCODE_O] == 1;
		}break;
		case BUTTON_R1:{
			return g_keystate[SDL_SCANCODE_U] == 1;
		}break;
		case BUTTON_START:{
			return g_keystate[SDL_SCANCODE_SPACE] == 1;
		}break;
		case BUTTON_LEFT:{
			return g_keystate[SDL_SCANCODE_A] == 1;
		}break;
		case BUTTON_RIGHT:{
			return g_keystate[SDL_SCANCODE_D] == 1;
		}break;
		case BUTTON_UP:{
			return g_keystate[SDL_SCANCODE_W] == 1;
		}break;
		case BUTTON_DOWN:{
			return g_keystate[SDL_SCANCODE_S] == 1;
		}break;
		default:
		break;
	}
	return 0;
}

void update_keys()
{
	for(int i=0;i<NUM_BUTTONS;i++)
	{
		if(is_controller_key_down(i) || is_keyboard_key_down(i))
		{
			if(g_keys_down[i])
			{
				g_keys_pressed[i] = 0;
			}
			else
			{
				g_keys_pressed[i] = 1;
			}
			g_keys_down[i] = 1;
			g_keys_released[i] = 0;
		}
		else
		{
			if(g_keys_down[i])
			{
				g_keys_released[i] = 1;
			}
			else
			{
				g_keys_released[i] = 0;
			}
			g_keys_down[i] = 0;
			g_keys_pressed[i] = 0;
		}
	}
}

int is_key_down(Button button)
{	
	return g_keys_down[button];
}

int is_key_pressed(Button button)
{
	return g_keys_pressed[button];
}

int is_key_released(Button button)
{
	return g_keys_released[button];
}

void add_letter(Font* font, char letter, int x, int y, int width, int height)
{
	font->letters[(int)letter].x = x;
	font->letters[(int)letter].y = y;
	font->letters[(int)letter].w = width;
	font->letters[(int)letter].h = height;
}

int font_create_default(Font* font)
{
	font->texture = texture_load("data/font.bmp");
	if(!font->texture)
		return 0;
	
	add_letter(font,' ', 0*8,0*8,8,8);
	add_letter(font,'!', 1*8,0*8,8,8);
	add_letter(font,'"', 2*8,0*8,8,8);
	add_letter(font,'#', 3*8,0*8,8,8);
	add_letter(font,'$', 4*8,0*8,8,8);
	add_letter(font,'%', 5*8,0*8,8,8);	
	add_letter(font,'&', 6*8,0*8,8,8);	
	//add_letter(font,'´', 7*8,0*8,8,8);
	add_letter(font,'(', 8*8,0*8,8,8);
	add_letter(font,')', 9*8,0*8,8,8);
	add_letter(font,'*', 10*8,0*8,8,8);
	add_letter(font,'+', 11*8,0*8,8,8);
	add_letter(font,',', 12*8,0*8,8,8);
	add_letter(font,'-', 13*8,0*8,8,8);
	add_letter(font,'.', 14*8,0*8,8,8);
	add_letter(font,'/', 15*8,0*8,8,8);
	
	add_letter(font,'0', 0*8, 1*8,8,8);
	add_letter(font,'1', 1*8, 1*8,8,8);
	add_letter(font,'2', 2*8, 1*8,8,8);
	add_letter(font,'3', 3*8, 1*8,8,8);
	add_letter(font,'4', 4*8, 1*8,8,8);
	add_letter(font,'5', 5*8, 1*8,8,8);	
	add_letter(font,'6', 6*8, 1*8,8,8);	
	add_letter(font,'7', 7*8, 1*8,8,8);
	add_letter(font,'8', 8*8, 1*8,8,8);
	add_letter(font,'9', 9*8, 1*8,8,8);
	add_letter(font,':', 10*8,1*8,8,8);
	add_letter(font,';', 11*8,1*8,8,8);
	add_letter(font,'<', 12*8,1*8,8,8);
	add_letter(font,'=', 13*8,1*8,8,8);
	add_letter(font,'>', 14*8,1*8,8,8);
	add_letter(font,'?', 15*8,1*8,8,8);
	
	add_letter(font,'@', 0*8, 2*8,8,8);
	add_letter(font,'A', 1*8, 2*8,8,8);
	add_letter(font,'B', 2*8, 2*8,8,8);
	add_letter(font,'C', 3*8, 2*8,8,8);
	add_letter(font,'D', 4*8, 2*8,8,8);
	add_letter(font,'E', 5*8, 2*8,8,8);	
	add_letter(font,'F', 6*8, 2*8,8,8);	
	add_letter(font,'G', 7*8, 2*8,8,8);
	add_letter(font,'H', 8*8, 2*8,8,8);
	add_letter(font,'I', 9*8, 2*8,8,8);
	add_letter(font,'J', 10*8,2*8,8,8);
	add_letter(font,'K', 11*8,2*8,8,8);
	add_letter(font,'L', 12*8,2*8,8,8);
	add_letter(font,'M', 13*8,2*8,8,8);
	add_letter(font,'N', 14*8,2*8,8,8);
	add_letter(font,'O', 15*8,2*8,8,8);
	
	add_letter(font,'P', 0*8, 3*8,8,8);
	add_letter(font,'Q', 1*8, 3*8,8,8);
	add_letter(font,'R', 2*8, 3*8,8,8);
	add_letter(font,'S', 3*8, 3*8,8,8);
	add_letter(font,'T', 4*8, 3*8,8,8);
	add_letter(font,'U', 5*8, 3*8,8,8);	
	add_letter(font,'V', 6*8, 3*8,8,8);	
	add_letter(font,'W', 7*8, 3*8,8,8);
	add_letter(font,'X', 8*8, 3*8,8,8);
	add_letter(font,'Y', 9*8, 3*8,8,8);
	add_letter(font,'Z', 10*8,3*8,8,8);
	add_letter(font,'[', 11*8,3*8,8,8);
	add_letter(font,'\\', 12*8,3*8,8,8);
	add_letter(font,']', 13*8,3*8,8,8);
	add_letter(font,'^', 14*8,3*8,8,8);
	add_letter(font,'_', 15*8,3*8,8,8);
	
	add_letter(font,'\'', 0*8, 4*8,8,8);
	add_letter(font,'a', 1*8, 4*8,8,8);
	add_letter(font,'b', 2*8, 4*8,8,8);
	add_letter(font,'c', 3*8, 4*8,8,8);
	add_letter(font,'d', 4*8, 4*8,8,8);
	add_letter(font,'e', 5*8, 4*8,8,8);	
	add_letter(font,'f', 6*8, 4*8,8,8);	
	add_letter(font,'g', 7*8, 4*8,8,8);
	add_letter(font,'h', 8*8, 4*8,8,8);
	add_letter(font,'i', 9*8, 4*8,8,8);
	add_letter(font,'j', 10*8,4*8,8,8);
	add_letter(font,'k', 11*8,4*8,8,8);
	add_letter(font,'l', 12*8,4*8,8,8);
	add_letter(font,'m', 13*8,4*8,8,8);
	add_letter(font,'n', 14*8,4*8,8,8);
	add_letter(font,'o', 15*8,4*8,8,8);
	
	add_letter(font,'p', 0*8, 5*8,8,8);
	add_letter(font,'q', 1*8, 5*8,8,8);
	add_letter(font,'r', 2*8, 5*8,8,8);
	add_letter(font,'s', 3*8, 5*8,8,8);
	add_letter(font,'t', 4*8, 5*8,8,8);
	add_letter(font,'u', 5*8, 5*8,8,8);	
	add_letter(font,'v', 6*8, 5*8,8,8);	
	add_letter(font,'w', 7*8, 5*8,8,8);
	add_letter(font,'x', 8*8, 5*8,8,8);
	add_letter(font,'y', 9*8, 5*8,8,8);
	add_letter(font,'z', 10*8,5*8,8,8);
	add_letter(font,'{', 11*8,5*8,8,8);
	add_letter(font,'|', 12*8,5*8,8,8);
	add_letter(font,'}', 13*8,5*8,8,8);
	add_letter(font,'~', 14*8,5*8,8,8);	
	
	return 1;
}
