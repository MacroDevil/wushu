#ifndef _INTRO_STATE_H
#define _INTRO_STATE_H

int init_intro_state();
void detroy_intro_state();

int update_intro_state(float delta);
void render_intro_state();

#endif