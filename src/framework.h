#ifndef _FRAMEWORK_H
#define _FRAMEWORK_H

//====================================
//INCLUDES
//====================================
#include <SDL2/SDL.h>
#include <stdio.h>
#include <stdint.h>

//====================================
//TYPES
//====================================
typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef int8_t s8;
typedef int16_t s16;
typedef int32_t s32;
typedef int64_t s64;

typedef unsigned char uchar;
typedef unsigned short ushort;
typedef unsigned int uint;

//====================================
//KEYS
//====================================
typedef enum
{
	BUTTON_A,
	BUTTON_B,
	BUTTON_X,
	BUTTON_Y,
	BUTTON_L1,
	BUTTON_R1,
	BUTTON_START,
	BUTTON_LEFT,
	BUTTON_RIGHT,
	BUTTON_UP,
	BUTTON_DOWN,
	NUM_BUTTONS
}Button;

int is_key_down(Button button);
int is_key_pressed(Button button);
int is_key_released(Button button);

//====================================
//PROTOTYPES
//====================================
int engine_init(int width, int height);
void engine_destroy();
void update_events();
void update_keys();
int should_close();
void start_frame();
void end_frame();

//====================================
//TIMER
//====================================
typedef struct
{
	uint paused_ticks;
	uint started_ticks;
	int  is_started;
	int  is_paused;
}Timer;

Timer timer_create();
void timer_start(Timer* timer);
void timer_stop(Timer* timer);
void timer_pause(Timer* timer);
void timer_unpause(Timer* timer);
uint timer_get_ticks(Timer* timer);

//====================================
//DRAWING
//====================================
SDL_Texture* texture_load(const char* filename);

void draw_rect(SDL_Rect rect, SDL_Color color);
void draw_filledrect(SDL_Rect rect, SDL_Color color);
void draw_sprite(SDL_Texture* texture, SDL_Rect src, SDL_Rect dst);

typedef struct
{
	SDL_Texture* texture;
	SDL_Rect letters[256];
}Font;
int font_create_default(Font* font);
void draw_text(const char* text, Font* font, int x, int y);

#endif
