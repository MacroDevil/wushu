#ifndef _ASSETS_H
#define _ASSETS_H

#include "framework.h"

Font* get_font();

int assets_init();
void assets_destroy();

#endif