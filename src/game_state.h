#ifndef _GAME_STATE_H
#define _GAME_STATE_H

int init_game_state();
void destroy_game_state();
int update_game_state(float delta);
void render_game_state();

#endif