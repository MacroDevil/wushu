#ifndef _MENU_STATE_H
#define _MENU_STATE_H

int init_menu_state();
void destroy_menu_state();

int update_menu_state(float delta);
void render_menu_state();

#endif