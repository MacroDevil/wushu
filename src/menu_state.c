#include "menu_state.h"

int g_cur_menu_index;

int init_menu_state()
{
	g_cur_menu_index = 0;
	
	return 1;
}

void destroy_menu_state()
{
}

int update_menu_state(float delta)
{
	if(is_key_pressed(BUTTON_DOWN))
	{
		g_cur_menu_index++;
		if(g_cur_menu_index > 1)
			g_cur_menu_index = 0;
	}
	
	if(is_key_pressed(BUTTON_UP))
	{
		g_cur_menu_index--;
		if(g_cur_menu_index < 0)
			g_cur_menu_index = 1;
	}
	
	if(is_key_pressed(BUTTON_A) || is_key_pressed(BUTTON_X) || is_key_pressed(BUTTON_START))
	{
		if(g_cur_menu_index == 0)
		{
			return 0;
		}
		else if(g_cur_menu_index == 1)
		{
			return 1;
		}
	}
	
	return -1;
}

void render_menu_state()
{
	draw_text("New Game",&g_font,30,10);
	draw_text("Exit",&g_font,30,20);
	if(g_cur_menu_index == 0)
	{
		draw_text(">",&g_font,20,10);
	}
	else if(g_cur_menu_index == 1)
	{
		draw_text(">",&g_font,20,20);
	}
}